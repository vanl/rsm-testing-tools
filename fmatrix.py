# -*- coding: utf-8 -*-
#!/usr/local/bin/python
"""
fmatrix.py: loading sparse feature matrix.
$Id: fmatrix.py,v 1.1 2013/06/28 08:06:00 daichi Exp $

"""

import numpy as np


def get_file_lines(filename):
    with open(filename, 'r') as fh:
        return sum(1 for _ in fh)


def parse2(filename, dictionary):
    """
    build a numpy full matrix from sparse 'id:cnt' data.
    """
    dictionary_size = get_file_lines(dictionary)
    file_lines = get_file_lines(filename)
    d = 0
    matrix = np.zeros((file_lines, dictionary_size))
    did = np.zeros(file_lines)
    with open(filename, 'r') as fh:
        for line in fh:
            tokens = line.split()
            if len(tokens) > 0:
                did[d] = tokens[0]
                for token in tokens[1:]:
                    [id, cnt] = token.split(':')
                    v = int(id) - 1
                    c = float(cnt)
                    matrix[d, v] = c
                d += 1
    return [did, matrix]


def parse(filename, dictionary):
    """
    take only matrix from those two values returned from parse2(...) method
    """
    [did, matrix] = parse2(filename, dictionary)
    return matrix
