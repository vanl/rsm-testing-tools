# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import print_function
import re
import sys
from fmatrix import get_file_lines

if __name__ == '__main__':
	if len(sys.argv) <= 2:
		print("usage: % parser.py input_file dictionary")
		sys.exit(0)
	else:
		file = sys.argv[1]
		dictionary = sys.argv[2]

	with open(file, 'r') as f:
		content = f.read()
	
	with open('dictionary.txt') as f:
		dictionary = dict(zip(filter(None, f.read().split('\n')),xrange(1, get_file_lines(dictionary))))

	matches = re.finditer(r"\.I ([0-9]+)\n\.W\n((?:\w+\s?)+\n)\n", content, re.DOTALL)
	for match in matches:
		print(match.group(1), end=' ')
		bag_of_words = {}
		for word in re.findall(r"[\w']+", match.group(2)):
			if word in bag_of_words:
				bag_of_words[word] += 1
			else:
				bag_of_words[word] = 1
		for word in bag_of_words:
			if word in dictionary:
				print(str(dictionary[word]) + ":" + str(bag_of_words[word]),end=' ')
		else:
			print(end='\n')

