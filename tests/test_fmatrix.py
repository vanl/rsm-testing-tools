# -*- coding: utf-8 -*-
#!/usr/bin/env python

import unittest
import fmatrix


class TestFMatrix(unittest.TestCase):
    def setUp(self):
        pass

    def test_parse2(self):
        [did, matrix] = fmatrix.parse2('tests/train', 'tests/train.lex')
        print did
        self.assertIsNotNone(did)
        self.assertEqual(did[0], 1)
        self.assertEqual(did[1], 2)
        self.assertEqual(did[2], 3)
        self.assertEqual(did[3], 4)

    def test_get_file_lines(self):
        self.assertEqual(fmatrix.get_file_lines('tests/train.lex'), 1324)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
