# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import division
import re
import sys
import os.path
import cPickle as pickle
import fmatrix
from rsm_numpy import sigmoid
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import time


def get_categories_dictionary(category_file):
    with open(category_file, 'r') as file:
        categories = file.read()

    categories_dictionary = {}
    matches = re.finditer(r"(\w+) ([0-9]+) 1", categories)
    for match in matches:
        category_id = match.group(1)
        document_id = int(match.group(2))
        if document_id in categories_dictionary:
            categories_dictionary[document_id].append(category_id)
        else:
            categories_dictionary[document_id] = [category_id]
    return categories_dictionary


def get_vectors_length(matrix):
    return np.array(map(np.linalg.norm, matrix))


def cosine_distance(a, b, norm_b = None):
    if norm_b is None:
        return np.dot(a, b) / np.dot(np.linalg.norm(a), np.linalg.norm(b))
    return np.dot(a, b) / np.dot(np.linalg.norm(a), norm_b)


def cosine_distance_closure(b, norm_b = None):
    def fun(a):
        if norm_b is None:
            return np.dot(a, b) / np.dot(np.linalg.norm(a), np.linalg.norm(b))
        return np.dot(a, b) / np.dot(np.linalg.norm(a), norm_b)
    return fun


def belongs_to_same_category(categories, id1, id2):
    if id1 not in categories or id2 not in categories:
        return False
    for i in categories[id1]:
        if i in categories[id2]:
            return True
    return False


def get_precision_and_recall_function(list):
    counted = [0]*len(list)
    for i in xrange(1, len(list)):
        counted[i] = counted[i-1] + (1 if list[i] else 0)

    def fun(limit):
        if limit >= len(list):
            limit = len(list)-1
        true_positives = counted[limit]
        precision = true_positives / limit
        recall = true_positives / counted[len(list)-1]
        return [precision, recall]
    return fun


def draw_chart(recall, precision, outputfile):
    plt.plot(recall, precision, '-')
    plt.legend(['RSM'], loc='best')
    plt.xscale('log')
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.savefig(outputfile)


def main(categories_file, model_file, test, dictionary, output_file):
    categories_cache = '.category.cache'
    protocol = 1

    if os.path.isfile(categories_cache):
        with open(categories_cache, 'r') as file:
            categories_dictionary = pickle.load(file)
    else:
        categories_dictionary = get_categories_dictionary(categories_file)
        with open(categories_cache, 'wb') as file:
            pickle.dump(categories_dictionary, file, protocol)

    with open(model_file, 'r') as fh:
        model = pickle.load(fh)
    w_vh = model['w_vh']
    w_v = model['w_v']
    w_h = model['w_h']

    [TEST_SET_IDS, TEST_SET] = fmatrix.parse2(test, dictionary)
    b_lengths = get_vectors_length(TEST_SET)

    global_precision = [0.0]*201
    global_recall = [i*0.005 for i in xrange(0, 201)]
    global_recall[0] = 0.001

    test_set_size = TEST_SET.shape[0]

    h = np.zeros(shape=(test_set_size, w_h.size))
    for i in xrange(test_set_size):
        b = TEST_SET[i]
        h[i] = np.array(sigmoid(np.dot(b, w_vh) + np.sum(b) * w_h))

    for i in xrange(test_set_size):
        print "index: ", i

        cosine_function = cosine_distance_closure(h[i], b_lengths[i])
        cos_dist = np.array(map(cosine_function, h))

        sorted_list = [j[0] for j in sorted(zip(range(TEST_SET_IDS.size), cos_dist), key=lambda l: l[1], reverse=True)]
        same_category = [belongs_to_same_category(categories_dictionary, TEST_SET_IDS[i], TEST_SET_IDS[j]) for j in sorted_list]

        precision_and_recall_function = get_precision_and_recall_function(same_category)

        precision = []
        recall = []
        for j in xrange(1, len(same_category)):
            [prec, rec] = precision_and_recall_function(j)
            precision.append(prec)
            recall.append(rec)
        f = interp1d(recall, precision)

        for j in xrange(0, 201):
            global_precision[j] = global_precision[j] + f(global_recall[j])

    global_precision = [x / test_set_size for x in global_precision]

    draw_chart(global_recall, global_precision, output_file)


if __name__ == '__main__':

    if len(sys.argv) <= 4:
        print("usage: % stats.py category_file model test dictionary output_file_with_chart")
        sys.exit(0)
    else:
        categories_file = sys.argv[1]
        model_file = sys.argv[2]
        test = sys.argv[3]
        dictionary = sys.argv[4]

    output_file = 'precision_recall.png'
    if len(sys.argv) == 6:
        output_file = sys.argv[5]

    plotting_time = 0.
    start_time = time.clock()

    main(categories_file, model_file, test, dictionary, output_file)

    end_time = time.clock()
    pretraining_time = (end_time - start_time)
    print ('Generating stats took %f minutes' % (pretraining_time / 60.))

